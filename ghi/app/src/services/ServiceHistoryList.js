import React from 'react';

class ServiceHistoryList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            searchAppointments: [],
            automobiles: [],
            selectedVin: '',
        };
        this.handleSearch = this.handleSearch.bind(this)
        this.handleAutomobileChange = this.handleAutomobileChange.bind(this);
    }

    handleAutomobileChange(e) {

        const value = e.target.value;
        this.setState({ selectedVin: value })
    }

    async handleSearch(event) {
        const value = this.state.selectedVin;
        console.log(this.state.selectedVin)
        const searchUrl = `http://localhost:8080/api/vin/appointment/${value}/`;
        const searchResponse = await fetch(searchUrl);
        if (searchResponse.ok) {
            const searchData = await searchResponse.json();
            this.setState({ searchAppointments: searchData });
        }
    }

    async componentDidMount() {
        const appointmentUrl = 'http://localhost:8080/api/appointments/';
        const autoUrl = 'http://localhost:8100/api/automobiles/';
        const appointmentResponse = await fetch(appointmentUrl)
        const autoResponse = await fetch(autoUrl);
        if (appointmentResponse.ok) {
            const appointmentData = await appointmentResponse.json()
            const autoData = await autoResponse.json();
            this.setState({ appointments: appointmentData })
            this.setState({ automobiles: autoData.autos })
        }
    }

    render() {
        return (
            <>
                <div className="px-4 py-5 my-1 mt-0 text-center">
                    <h1 className="display-5 fw-bold">Service Appointment History</h1>
                </div>
                <div className="container">
                    <div className="row height d-flex justify-content-center align-items-center">
                        <div className="col-md-6">
                            <div className="input-group mb-3">
                                <select onChange={this.handleAutomobileChange} value={this.state.automobiles}  required id="automobile" name="automobile" className="form-select">
                                    <option value="">VIN #</option>
                                    {this.state.automobiles.map(automobile => {
                                        return (
                                            <option key={automobile.vin} value={automobile.vin}>{automobile.vin}</option>
                                        );
                                    })}
                                </select>
                                <button onClick={this.handleSearch} className="btn btn-primary" id="searchBtn" type="submit">Search</button>
                            </div>
                        </div>
                    </div>
                </div>
                <table className="table table-striped my-5">
                    <thead>
                        <tr>
                            <th>VIN</th>
                            <th>Customer Name</th>
                            <th>Date</th>
                            <th>Time</th>
                            <th>Technician</th>
                            <th>Reason</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.searchAppointments.map(appoint => { console.log(appoint)
                            return ( 
                                <tr key={appoint.id}>
                                    <td id="appointmentData">{appoint.vehicle.vin}</td>
                                    <td id="appointmentData">{appoint.owner}</td>
                                    <td id="appointmentData">{appoint.date}</td>
                                    <td id="appointmentData">{appoint.time}</td>
                                    <td id="appointmentData">{appoint.technician.name}</td>
                                    <td id="appointmentData">{appoint.reason}</td>
                                </tr>
                            );
                        })}
                    </tbody>
                </table>
            </>
        );
    }
}

export default ServiceHistoryList;

